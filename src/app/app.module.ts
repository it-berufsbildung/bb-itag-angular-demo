import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './mainApp/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BbItagAngularNavbarModule } from 'bb-itag-angular-navbar';
import { BbItagAngularFooterModule } from 'bb-itag-angular-footer';
import { BbItagAngularChangeLogModule } from 'bb-itag-angular-change-log';
import { DemoComponent } from './mainApp/demo/demo.component';
import { MyChangeLogComponent } from './mainApp/my-change-log/my-change-log.component';
import { MatButtonModule, MatCardModule, MatInputModule, MatSlideToggleModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DemoComponent,
    MyChangeLogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    MatButtonModule,
    MatSlideToggleModule,
    MatInputModule,
    BrowserAnimationsModule,
    BbItagAngularNavbarModule,
    BbItagAngularFooterModule,
    BbItagAngularChangeLogModule,
    MatCardModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
