import { faGrin, faSmile, faToolbox } from '@fortawesome/free-solid-svg-icons';
import { IMenuItem } from 'bb-itag-angular-navbar';


const spacerIsAuthenticated: IMenuItem = {
  name: '-',
  roles: ['.'],
};
const spacerIsAdmin: IMenuItem = {
  name: '-',
  roles: ['isAdmin'],
};
const spacerIsUser1: IMenuItem = {
  name: '-',
  roles: ['isUser1'],
};
const spacerIsUser2: IMenuItem = {
  name: '-',
  roles: ['isUser2'],
};
const spacerIsUser3: IMenuItem = {
  name: '-',
  roles: ['isUser3'],
};
const menuItemSpacer: IMenuItem = {
  name: '-',
  roles: [],
};

const submenu1: IMenuItem = {
  name: 'submenu 1',
  routerLink: '/authenticated/submenu1',
  roles: ['isUser1'],
  faIcon: faGrin,
};
const submenu2: IMenuItem = {
  name: 'submenu 2',
  routerLink: '/authenticated/submenu2',
  faIcon: faSmile,
  faSize: '1x',
  roles: ['isUser2'],
};
const submenu3: IMenuItem = {
  name: 'submenu 3',
  routerLink: '/authenticated/submenu3',
  faIcon: faSmile,
  faSize: '1x',
  roles: ['isUser3'],
};
const menuItemAdmin: IMenuItem = {
  name: 'Admins',
  routerLink: '/admin',
  roles: ['isAdmin'],
  faIcon: faToolbox,
  faSize: '1x'
};

const menuItemAuthentication: IMenuItem = {
  name: 'Authenticated',
  roles: ['.'],
  childMenuItems: [
    submenu1,
    spacerIsUser2,
    submenu2,
    spacerIsUser3,
    submenu3,
    spacerIsAdmin,
    menuItemAdmin
  ]
};
const menuItemSub2: IMenuItem = {
  name: 'Sub2',
  faIcon: faToolbox,
  roles: ['isUser1', 'isUser2', 'isUser3'],
  childMenuItems: [
    submenu1,
    spacerIsUser1,
    submenu2,
    spacerIsUser2,
    submenu3
  ]
};
const menuItemHelpAbout: IMenuItem = {
  name: 'help',
  roles: [],
  routerLink: '/help/about'
};
const menuItemHelpChangelog: IMenuItem = {
  name: 'changelog',
  roles: [],
  routerLink: 'help/changelog'
};
const menuItemHelp: IMenuItem = {
  name: 'help',
  roles: [],
  childMenuItems: [
    menuItemHelpChangelog,
    menuItemSpacer,
    menuItemHelpAbout
  ]
};

export const leftMenuItems: IMenuItem[] = [
  menuItemAuthentication,
  submenu1,
  submenu2,
  submenu3,
  menuItemSub2,
];
export const rightMenuItems: IMenuItem[] = [
  menuItemAdmin,
  menuItemHelp
];

