import { Component } from '@angular/core';
import { ThemePalette } from '@angular/material';
import { NavigationEnd, Router, RouterEvent } from '@angular/router';
import { faAddressBook, faTable } from '@fortawesome/free-solid-svg-icons';
import { of } from 'rxjs';
import { ConnectionService } from 'ng-connection-service';
import { leftMenuItems, rightMenuItems } from './menuConfiguration';
import { IBaseMenuInfo, IMenuItem, IProfileMenuItem, IsMenuVisibleMethod } from 'bb-itag-angular-navbar';
import { AuthService } from '../services/auth.service';
// @ts-ignore
import * as pjson from '../../../package.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    connectionService: ConnectionService,
    router: Router,
    private authService: AuthService,
  ) {

    this.setColor('primary');

    connectionService.monitor()
      .subscribe( (value: boolean) => {
        this.setOnline(value);
      });

    router.events.subscribe( (val: RouterEvent) => {
        // console.dir(val);
        if (val instanceof NavigationEnd) {
          this.url = val.url;
        }
      }
    );
    this.setUsername();
  }

  public url: string;
  public online = true;
  public showProfile = true;
  public username = 'GUEST';

  private profileMenuItem: IProfileMenuItem = {
    name: 'profile',
    showProfile: this.showProfile,
    routerLink: '/profile',
    username$: this.authService.username$,
    isAuthenticated$: of(true), // this.authService.isAuthenticated$,
  };

  // -----
  // header
  // ------
  // the material color settings warn, accent or primary (default 'primary')
  public color: ThemePalette;
  // the array with the menus, showing left (default [])
  public leftMenuItems: IMenuItem[] = leftMenuItems;
  // the array with the menus, showing right (default [])
  public rightMenuItems: IMenuItem[] = rightMenuItems;
  // the name we show on the top of the menu (default: 'myApp')
  public applicationName = pjson.name;
  // we strongly recommend to add a alternate name here in case you have an uri set (default: 'the logo of the company')
  public applicationLogoAlt = 'Logo of the application!';
  // the logo to the image we show on the top left of the menu (recommended size: 120x30px) ) (default: null)
  public applicationLogoUri = '/assets/images/logo.png';
  // how we should show the profile menu (default: username$ = of('test'), isAuthenticated$ = of(true), showProfile = true)
  public profile: IProfileMenuItem = this.profileMenuItem;
  // what global css class we have to use to show the active menu (default: 'empty', this is an internal class)
  public activeRouterLinkClass = 'activeRouterLinkColor';
  // do we have to log some debug information like menu click, or isVisible checks (default: false)
  public showDebug = false;
  // the router link on the application name / logo on the left. (default /)
  public applicationRouterLink = '/';
  // set the property to the method to call (default to a internal method that returns always of(true))
  public isMenuVisibleMethod: IsMenuVisibleMethod = this.isMenuVisible(this.authService);

  // -----
  // footer
  // ------
  // the application version which gets displayed at the bottom left (default: null)
  public appVersion = `Version: ${pjson.version}`;
  // boolean if the cursor should be an pointer or an arrow. Needs to be true to use routerLink and click event (default: true)
  public showPointer = true;
  // the material color settings warn, accent or primary (default 'warn') of the footer when it's offline
  public offlineThemePalette: ThemePalette = 'warn';
  // the font awesome icon that gets displayed when you're online (default: 'faNetworkWired')
  public onlineFaIconDefinition: any = faAddressBook;
  // the font awesome icon that gets displayed when you're offline (default: 'faExclamationTriangle')
  public offlineFaIconDefinition: any = faTable;
  // the text that gets displayed at the bottom right when you're online (default: 'ONLINE')
  public onlineText = 'connected';
  // the text that gets displayed at the bottom right when you're offline (default: 'OFFLINE')
  public offlineText = 'disconnected';
  // the router link on the version / text on the left. (default /)
  public versionRouterLink = '/help/changelog';

  private isMenuVisible( authService: AuthService ): any {
    return (menuItem: IMenuItem) => {
      // console.log(menuItem.roles);
      if (menuItem.roles.length === 0) {
        return of(true);
      }
      return authService.isMemberOf(menuItem.roles);
    };
  }

  public onFooterAppVersionClicked(version: string) {
    console.log(version);
  }

  public setColor( color: ThemePalette ) {
    this.color = color;
    switch (color ) {
      case 'primary':
        this.activeRouterLinkClass = 'active_primary';
        this.offlineThemePalette = 'warn';
        break;
      case 'warn':
        this.activeRouterLinkClass = 'active_warn';
        this.offlineThemePalette = 'accent';
        break;
      case 'accent':
        this.activeRouterLinkClass = 'active_accent';
        this.offlineThemePalette = 'primary';
    }
  }

  public setOnline( online: boolean ) {
    this.online = online;
  }

  setUsername() {
    this.authService.setUsername(this.username);
  }

  switchShowProfile( checked: boolean ) {
    this.showProfile = checked;
    this.profile.showProfile = checked;
    this.username = null;
    this.setUsername();
  }

  public onMenuClicked(menuItem: IBaseMenuInfo) {
    alert (menuItem.name);
  }
}
