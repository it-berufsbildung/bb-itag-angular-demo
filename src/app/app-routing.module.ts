import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DemoComponent } from './mainApp/demo/demo.component';
import { MyChangeLogComponent } from './mainApp/my-change-log/my-change-log.component';

const routes: Routes = [
  { path: 'help/changelog', component: MyChangeLogComponent},
  { path: '**', component: DemoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
