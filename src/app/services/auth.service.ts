import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  private usernameSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);
  public username$: Observable<string> = this.usernameSubject.asObservable()
  private username: string;

  public isAdmin = false;
  public isUser1 = false;
  public isUser2 = false;
  public isUser3 = false;

  public get isAuthenticated$(): Observable<boolean> {
    return of( this.isAuthenticated() );
  }

  public setUsername(username: string) {
    this.usernameSubject.next(username);
    this.username = username;
  }
  public setIsAdmin(isActive: boolean) {
    this.isAdmin = isActive;
  }
  public setIsUser1(isActive: boolean) {
    this.isUser1 = isActive;
  }
  public setIsUser2(isActive: boolean) {
    this.isUser2 = isActive;
  }
  public setIsUser3(isActive: boolean) {
    this.isUser3 = isActive;
  }

  public isMemberOf( roles: string[] ): Observable<boolean> {
    // console.log(menuItem.roles);
    // prepare the list of active roles
    const activeRoles: string[] = [];
    if ( this.isAdmin ) {
      activeRoles.push ( 'isAdmin' );
    }
    if ( this.isUser1 ) {
      activeRoles.push ( 'isUser1' );
    }
    if ( this.isUser2 ) {
      activeRoles.push ( 'isUser2' );
    }
    if ( this.isUser3 ) {
      activeRoles.push ( 'isUser3' );
    }

    if (activeRoles.length === 0) {
      if ( this.isAuthenticated() && roles.indexOf('.') > -1) { return of(true); }
    }

    // tslint:disable-next-line:forin
    for ( const i in activeRoles ) {
      // authenticated is enough
      if ( roles.indexOf ( '.' ) > -1 ) {
        return of ( true );
      }
      const oldRoleName = activeRoles[ i ];
      if ( roles.indexOf ( oldRoleName ) > -1 ) {
        return of ( true );
      }
    }
    return of ( false );
  }

  public setMembership(role: 'admin' | 'user1' | 'user2' | 'user3', value: boolean ) {
    switch ( role ) {
      case 'admin':
        this.setIsAdmin(value);
        break;
      case 'user1':
        this.setIsUser1(value);
        break;
      case 'user2':
        this.setIsUser2(value);
        break;
      case 'user3':
        this.setIsUser3(value);
        break;
    }
  }

  private isAuthenticated(): boolean {
    return (this.username && this.username.length > 0) || this.isUser1 || this.isUser2 || this.isUser3 || this.isAdmin;
  }

}
